# DSpace setup

## All in one (tomcat9, postgresql-12, dspace-6.3):

`bash ./setup.sh`

Step by step (what [./setup.sh](./setup.sh) does):

- Requirements:
  - Java Application server : tomcat9
  - Database: Postgresql-12
  - jdk8

- Update the reposiroties and install the required packages:

```bash
cd ~
sudo apt update
sudo apt install -y tomcat9 postgresql-12 openjdk-8-jdk-headless maven ant wget
sudo systemctl enable tomcat9 postgresql && sudo systemctl start tomcat9 postgresql
sudo update-alternatives --set java /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java
 ```

- Generate required (target) directories

```bash
sudo mkdir -p /val/log/tomcat9 && sudo chown -R tomcat:tomcat /var/log/tomcat9
sudo mkdir -p /usr/share/tomcat9/dspace && sudo chown -R tomcat:tomcat /usr/share/tomcat9/dspace
```

- Database setup (user/database/password)

```bash
# random password
DB_PASSWROD="$(cat /dev/urandom | tr -dc 'a-z' | fold -w 12 | head -n 1 )"
sudo -u postgres psql -c "CREATE USER dspace with PASSWORD '${DB_PASSWROD}';"
sudo -u postgres createdb  --owner=dspace --encoding=UNICODE dspace
sudo -u postgres psql dspace -c 'CREATE EXTENSION pgcrypto;'
```

- Download and extract Dspace

```bash
DSPACE_VERSION=6.3
wget "https://github.com/DSpace/DSpace/releases/download/dspace-${DSPACE_VERSION}/dspace-${DSPACE_VERSION}-release.tar.gz"
tar -xvf dspace-${DSPACE_VERSION}-release.tar.gz 
cd dspace-${DSPACE_VERSION}-release
```

- Modify configuration

```bash
cp dspace/config/local.cfg.EXAMPLE dspace/config/local.cfg
sed  -i 's|dspace.dir=.*|dspace.dir=/usr/share/tomcat9/dspace|'  dspace/config/local.cfg
sed  -i "s|db.password.*|db.password = ${DB_PASSWROD}|" dspace/config/local.cfg

sed -i 's|log.dir=.*|log.dir=/var/log/tomcat9|' dspace/config/log4j.properties
sed -i 's|log.dir=.*|log.dir=/var/log/tomcat9|' dspace/config/log4j-solr.properties
sed -i 's|log.dir=.*|log.dir=/var/log/tomcat9|' dspace/config/log4j-handle-plugin.properties
```

- Build packages and copy them to tomcat's webapps directory

```bash
mvn package
cd dspace/target/dspace-installer/
sudo ant fresh_install
cd ../../../
sudo chown -R tomcat:tomcat /usr/share/tomcat9/dspace
sudo cp -Rp /usr/share/tomcat9/dspace/webapps/* /var/lib/tomcat9/webapps/
```

- Modify tomcat's configuration/service, to overpass "read-only" error

```bash
if [ "$(grep -i "ReadWritePaths=/usr/share/tomcat9/dspace/" /lib/systemd/system/tomcat9.service 2>/dev/null || echo nope)" = "nope" ];then
    sudo sed -i "s/\[Install\]/ReadWritePaths=\/usr\/share\/tomcat9\/dspace\/\n[Install]/" /lib/systemd/system/tomcat9.service
    sudo systemctl daemon-reload && sudo systemctl restart tomcat9
fi
```

## Update confiiguration

 SMTP example:

```bash
nano ~/dspace-${DSPACE_VERSION}-release/dspace/target/dspace-installer/config/local.cfg
```

```bash
mail.server = smtp.gmail.com
mail.extraproperties = mail.smtp.socketFactory.port=587, \
                        mail.smtp.starttls.enable=true, \
                        mail.smtp.starttls.required=true, \
                        mail.smtp.ssl.protocols=TLSv1.2
# SMTP mail server authentication username and password (if required)
mail.server.username = user@gmail.com
mail.server.password = userspasswrod

# SMTP mail server alternate port (defaults to 25)
mail.server.port = 587

# From address for mail
# All mail from the DSpace site will use this 'from' address
mail.from.address = user@gmail.com
mail.admin = user@gmail.com
```

```bash
cd ~/dspace-${DSPACE_VERSION}-release/dspace/target/dspace-installer
sudo ant update_configs
# maybe not needed
sudo cp -rp /usr/share/tomcat9/dspace/webapps/* /var/lib/tomcat9/webapps/
sudo systemctl restart tomcat9
```

## Logs to watch:

- dspace:

```bash
# 500 last lines
sudo tail -n 500 /var/log/tomcat9/dspace.log*
# keep watching with the -f flag:
sudo tail -n 500 -t /var/log/tomcat9/dspace.log*
```

- tomcat:

```bash
# -e: end of logs, -u tomcat: display logs only of the tomcat9 unit
sudo journalctl -eu tomcat9
# again, use -f to "follow" the logs
sudo journalctl -efu tomcat9
```

## [Optional] Nginx reverse proxy

```bash
sudo apt install -y nginx && sudo nano /etc/nginx/sites-enabled/default
```

In the "server block" set sth like (proxy pass 8080):

```bash
  # only when on the root redirect to /jspui/
  location = / {
      return 301 /jspui/;
  }

  location / {
	  proxy_pass http://127.0.0.1:8080;
	  proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_connect_timeout                   10s;
        proxy_send_timeout                      360s;
        proxy_read_timeout                      360s;
    }

```

Install also certbot (`sudo snap install certbot`) to get a certificate and use sth like:

```bash
    ....
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/the.fqdn.com/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/the.fqdn.com/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
    ...
```

## Useful dspace commands
```bash
sudo /usr/share/tomcat9/dspace/bin/dspace create-administrator
sudo /usr/share/tomcat9/dspace/bin/dspace user --modify -m user1@example.com --newEmail user2@example.com
```
