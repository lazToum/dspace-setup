#!/bin/bash
#shellcheck disable=SC2002,SC2018,SC2164

DSPACE_VERSION=6.3
DB_PASSWROD="$(cat /dev/urandom | tr -dc 'a-z' | fold -w 12 | head -n 1 )"

sudo apt update
sudo apt install -y tomcat9 postgresql-12 openjdk-8-jdk-headless maven ant wget
sudo systemctl start tomcat9 postgresql && sudo systemctl start tomcat9 postgresql
sudo update-alternatives --set java /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java

sudo mkdir -p /val/log/tomcat9 && sudo chown -R tomcat:tomcat /var/log/tomcat9
sudo mkdir -p /usr/share/tomcat9/dspace && sudo chown -R tomcat:tomcat /usr/share/tomcat9/dspace

sudo -u postgres psql -c "CREATE USER dspace with PASSWORD '${DB_PASSWROD}';"
sudo -u postgres createdb  --owner=dspace --encoding=UNICODE dspace
sudo -u postgres psql dspace -c 'CREATE EXTENSION pgcrypto;'

wget "https://github.com/DSpace/DSpace/releases/download/dspace-${DSPACE_VERSION}/dspace-${DSPACE_VERSION}-release.tar.gz"
tar -xvf dspace-${DSPACE_VERSION}-release.tar.gz && cd dspace-${DSPACE_VERSION}-release
cp dspace/config/local.cfg.EXAMPLE dspace/config/local.cfg
sed  -i 's|dspace.dir=.*|dspace.dir=/usr/share/tomcat9/dspace|'  dspace/config/local.cfg
sed  -i "s|db.password.*|db.password = ${DB_PASSWROD}|" dspace/config/local.cfg

sed -i 's|log.dir=.*|log.dir=/var/log/tomcat9|' dspace/config/log4j.properties
sed -i 's|log.dir=.*|log.dir=/var/log/tomcat9|' dspace/config/log4j-solr.properties
sed -i 's|log.dir=.*|log.dir=/var/log/tomcat9|' dspace/config/log4j-handle-plugin.properties

mvn package
cd dspace/target/dspace-installer/
sudo ant fresh_install
cd ../../../
sudo chown -R tomcat:tomcat /usr/share/tomcat9/dspace
sudo cp -Rp /usr/share/tomcat9/dspace/webapps/* /var/lib/tomcat9/webapps/

if [ "$(grep -i "ReadWritePaths=/usr/share/tomcat9/dspace/" /lib/systemd/system/tomcat9.service 2>/dev/null || echo nope)" = "nope" ];then
    sudo sed -i "s/\[Install\]/ReadWritePaths=\/usr\/share\/tomcat9\/dspace\/\n[Install]/" /lib/systemd/system/tomcat9.service
    sudo systemctl daemon-reload && sudo systemctl restart tomcat9
fi
