# -*- mode: ruby -*-
 # vi: set ft=ruby :

## images
LIBVIRT_IMAGE = ENV["LIBVIRT_IMAGE"] || "generic/ubuntu2004"

IPS_PREFIX = ENV["IPS_PREFIX"] || "172.16.0."
NETMASK = ENV["NETMASK"] || "255.255.255.0"

# ssh provision
$before = <<SHELL
  sudo sed -i -E 's/^#?PasswordAuthentication no.*/PasswordAuthentication yes/g' /etc/ssh/sshd_config
  if [ -f /home/vagrant/.ssh/id_rsa ]; then 
      rm /home/vagrant/.ssh/id_rsa
  fi
  if [ -f /home/vagrant/.ssh/id_rsa.pub ]; then 
      rm /home/vagrant/.ssh/id_rsa.pub
  fi
  ssh-keygen -f /home/vagrant/.ssh/id_rsa -t rsa -q -N ''
  cat > /home/vagrant/.ssh/config <<EOF
Host *
  StrictHostKeyChecking no
  UserKnownHostsFile=/dev/null
EOF
chmod 600 /home/vagrant/.ssh/config && chown vagrant /home/vagrant/.ssh/config
if command -v systemctl  &> /dev/null; then
  if command -v apt &> /dev/null; then
    sudo systemctl restart ssh
  else
    sudo systemctl restart sshd
  fi
elif [ -f /etc/init.d/ssh ]; then
  sudo /etc/init.d/ssh restart
else
  sudo kill -HUP $(cat /var/run/sshd.pid)
fi
SHELL

LB_PORTS = [
  { "guest" => 80, "host" => 80 },
  { "guest" => 443, "host" => 443 },
  { "guest" => 8080, "host" => 8080 },
]

# retrieve the insecure public key
PUBLIC_KEY = %x[ssh-keygen -y -f ~/.vagrant.d/insecure_private_key | tr -d '\n' ]

# start vagrant config
VAGRANTFILE_API_VERSION = "2"
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  if Vagrant.has_plugin?("vagrant-hostmanager")
    # we handle /etc/hosts
    config.hostmanager.enabled = false
  end

  # no need to use /vagrant
  config.vm.synced_folder ".", "/vagrant", disabled: true

  # libvirt
  config.vm.provider "libvirt" do |libvirt, override|
    override.vm.box = LIBVIRT_IMAGE
    libvirt.graphics_type = "none"
    libvirt.video_type = "cirrus"
    libvirt.default_prefix = ""
  end
  config.vm.define "base" do |base|
    base.vm.network "private_network", ip: IPS_PREFIX + "100", netmask: NETMASK, hostname: true
    base.vm.hostname = "ubuntu.vm.local"
    LB_PORTS.each do|p|
      base.vm.network "forwarded_port", guest: p["guest"], host: p["host"], host_ip: "0.0.0.0"
    end
    base.vm.provision "shell", privileged: false, inline: $before, preserve_order: true
    base.vm.provision "shell",
      path: "setup.sh",
      privileged: false,
      preserve_order: true
    base.vm.provider "libvirt" do |libvirt|
      libvirt.cpus = 2
      libvirt.memory = 2048
    end
  end
end
